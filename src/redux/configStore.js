import { applyMiddleware, combineReducers, createStore } from "redux";
import thunk from "redux-thunk";
import { CarouselReducer } from "./reducer/CarouselReducer";
import { LoadingReducer } from "./reducer/LoadingReducer";
import { QuanLyDatVeReducer } from "./reducer/QuanLyDatVeReducer";
import { QuanLyNguoiDungReducer } from "./reducer/QuanLyNguoiDung";
import { QuanLyPhimReducer } from "./reducer/QuanLyPhimReducer";
import { QuanLyRapReducer } from "./reducer/QuanLyRapReducer";

const rootReducer = combineReducers({
  CarouselReducer,
  QuanLyPhimReducer,
  QuanLyRapReducer,
  QuanLyNguoiDungReducer,
  QuanLyDatVeReducer,
  LoadingReducer,
});
export const store = createStore(rootReducer, applyMiddleware(thunk));
