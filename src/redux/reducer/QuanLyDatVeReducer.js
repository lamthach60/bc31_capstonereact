import {
  CHANGE_TAB_ACTIVE,
  CHUYENTAB,
  DATVE,
  DATVEHOANTAT,
  LAYCHITIETPHONGVE,
} from "../constants/token";
import { ThongTinLichChieu } from "../../_core/models/ThongTinPhongVe";
const initialState = {
  chiTietPhongVe: new ThongTinLichChieu(),
  danhSachGheDangDat: [],
  tabActive: 1,
  danhSachGheKhachDat: [
    {
      maGhe: 86232,
    },
  ],
};

export const QuanLyDatVeReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case LAYCHITIETPHONGVE:
      state.chiTietPhongVe = payload;
      return { ...state };
    case DATVE:
      let danhSachGheCapNhat = [...state.danhSachGheDangDat];
      let index = danhSachGheCapNhat.findIndex(
        (gheDD) => gheDD.maGhe === payload.maGhe
      );

      if (index != -1) {
        danhSachGheCapNhat.splice(index, 1);
      } else {
        danhSachGheCapNhat.push(payload);
      }
      return { ...state, danhSachGheDangDat: danhSachGheCapNhat };
    case DATVEHOANTAT:
      state.danhSachGheDangDat = [];
      return { ...state };
    case CHUYENTAB:
      state.tabActive = 2;
      return { ...state };
    case CHANGE_TAB_ACTIVE:
      state.tabActive = payload;
      return { ...state };
    default:
      return state;
  }
};
