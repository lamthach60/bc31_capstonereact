import { USERLOGIN } from "../../services/configURL";
import { DANGNHAP, THONGTINNGUOIDUNG } from "../constants/token";
import { TOKEN } from "../constants/token";

let user = {};
if (localStorage.getItem(USERLOGIN)) {
  user = JSON.parse(localStorage.getItem(USERLOGIN));
}
const initialState = {
  userLogin: user,
  thongTinNguoiDung: {},
};

export const QuanLyNguoiDungReducer = (
  state = initialState,
  { type, payload }
) => {
  switch (type) {
    case DANGNHAP:
      state.userLogin = payload;
      localStorage.setItem(USERLOGIN, JSON.stringify(payload));
      localStorage.setItem(TOKEN, payload.accessToken);
      return { ...state, userLogin: payload };
    case THONGTINNGUOIDUNG:
      state.thongTinNguoiDung = payload;
      return { ...state };
    default:
      return state;
  }
};
