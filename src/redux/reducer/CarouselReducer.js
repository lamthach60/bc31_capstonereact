const stateDefault = {
  arrImg: [],
};

export const CarouselReducer = (state = stateDefault, { type, payload }) => {
  switch (type) {
    case "SET_CAROUSEL": {
      state.arrImg = payload;
      return { ...state };
    }
    default:
      return { ...state };
  }
};
