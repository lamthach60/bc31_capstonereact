import { DATVE, SETINFORAP } from "../constants/token";

const initialState = {
  heThongRapChieu: [],
};

export const QuanLyRapReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case SETINFORAP:
      state.heThongRapChieu = payload;
      return { ...state };

    default:
      return state;
  }
};
