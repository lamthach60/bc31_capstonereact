import {
  ADDPHIM,
  SETCHITIETPHIM,
  SETMOVIELIST,
  SETPHIMDANGCHIEU,
  SETPHIMSAPCHIEU,
  SETTHONGTINPHIM,
} from "../constants/token";

const initialState = {
  arrFilm: [],
  arrFilmDefault: [],
  dangChieu: true,
  sapChieu: true,
  filmDetail: {},
  thongTinPhim: {},
};

export const QuanLyPhimReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case SETMOVIELIST:
      state.arrFilm = payload;
      state.arrFilmDefault = state.arrFilm;
      return { ...state };
    case SETPHIMDANGCHIEU:
      state.dangChieu = !state.dangChieu;
      state.arrFilm = state.arrFilmDefault.filter(
        (film) => film.dangChieu === state.dangChieu
      );
      return { ...state };
    case SETPHIMSAPCHIEU:
      state.sapChieu = !state.sapChieu;
      state.arrFilm = state.arrFilmDefault.filter(
        (film) => film.sapChieu === state.sapChieu
      );
      return { ...state };
    case SETCHITIETPHIM:
      state.filmDetail = payload;
      return { ...state };
    case ADDPHIM:
      state.arrFilm = payload;
      return { ...state };
    case SETTHONGTINPHIM:
      state.thongTinPhim = payload;
      return { ...state };
    default:
      return state;
  }
};
