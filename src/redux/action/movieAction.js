import { ADDPHIM, SETMOVIELIST, SETTHONGTINPHIM } from "../constants/token";
import { movieService } from "../../services/movieService";
import { message } from "antd";
import { history } from "../../App";
export const layDanhSachPhim = (tenPhim = "") => {
  return (dispatch) => {
    movieService
      .layDanhSachPhim(tenPhim)
      .then((result) => {
        // console.log("result", result);
        dispatch({
          type: SETMOVIELIST,
          payload: result.data.content,
        });
      })
      .catch((err) => {
        console.log("err :", err);
      });
  };
};
export const themPhimUploadHinh = (frm) => {
  return (dispatch) => {
    movieService
      .themPhimUploadHinh(frm)
      .then((result) => {
        console.log("result :", result);
        dispatch({
          type: ADDPHIM,
          payload: result.data.content,
        });
        message.success(result.data.message);
        history.push("/admin/films");
      })
      .catch((err) => {
        console.log("err :", err);
        message.error(err.response.data.content);
      });
  };
};
export const layThongTinPhimAction = (maPhim) => {
  return (dispatch) => {
    movieService
      .layThongTinPhim(maPhim)
      .then((result) => {
        console.log("result", result.data.content);
        dispatch({
          type: SETTHONGTINPHIM,
          payload: result.data.content,
        });
      })
      .catch((err) => {
        console.log("err", err);
      });
  };
};
export const capNhatPhimUpload = (frm) => {
  return (dispatch) => {
    movieService
      .capNhatPhimUpload(frm)
      .then((result) => {
        console.log("result :", result);
        message.success(result.data.message);
        history.push("/admin/films");
      })
      .catch((err) => {
        console.log("err :", err);
        message.error(err.response.data.content);
      });
  };
};

export const xoaPhimAction = (maPhim) => {
  return (dispatch) => {
    movieService
      .xoaPhim(maPhim)
      .then((result) => {
        console.log("result :", result);
        message.success(result.data.message);
        dispatch(layDanhSachPhim());
        history.push("/admin/films");
      })
      .catch((err) => {
        console.log("err :", err);
      });
  };
};
