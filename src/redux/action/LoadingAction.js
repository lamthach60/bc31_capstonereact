import { DISPLAY_LOADING, HIDE_LOADING } from "../constants/token";

export const batLoadingAction = () => ({
  type: DISPLAY_LOADING,
});
export const tatLoadingAction = () => ({
  type: HIDE_LOADING,
});
