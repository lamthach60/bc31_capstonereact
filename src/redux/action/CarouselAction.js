import { movieService } from "../../services/movieService";
import { SET_CAROUSEL } from "../constants/token";

export const getCarouselAction = () => {
  return (dispatch) => {
    movieService
      .getBanner()
      .then((result) => {
        dispatch({
          type: SET_CAROUSEL,
          payload: result.data.content,
        });
      })
      .catch((err) => {});
  };
};
