import {
  CHUYENTAB,
  DATVE,
  DATVEHOANTAT,
  LAYCHITIETPHONGVE,
} from "../constants/token";
import { QuanLyDatVeService } from "../../services/QuanLyDatVeService";
import { ThongTinDatVe } from "../../_core/models/ThongTinDatVe";
import { batLoadingAction, tatLoadingAction } from "./LoadingAction";
export const quanLyDatVe = (maLichChieu) => {
  return (dispatch) => {
    QuanLyDatVeService.layChiTietPhongVe(maLichChieu)

      .then((result) => {
        console.log("result :", result);
        dispatch(batLoadingAction());
        dispatch({
          type: LAYCHITIETPHONGVE,
          payload: result.data.content,
        });
        dispatch(tatLoadingAction());
      })
      .catch((err) => {
        console.log("err :", err);
        dispatch(tatLoadingAction());
      });
  };
};
export const datVeAction = (thongTinDatVe = new ThongTinDatVe()) => {
  return (dispatch) => {
    QuanLyDatVeService.datVe(thongTinDatVe)
      .then((result) => {
        dispatch(batLoadingAction);
        console.log("result :", result);
        dispatch(quanLyDatVe(thongTinDatVe.maLichChieu));
        dispatch({ type: DATVEHOANTAT });
        dispatch({
          type: DATVE,
          payload: result.data.content,
        });
        dispatch(tatLoadingAction);
        dispatch({ type: CHUYENTAB });
      })
      .catch((err) => {
        console.log("err :", err.response);
        dispatch(tatLoadingAction);
      });
  };
};
