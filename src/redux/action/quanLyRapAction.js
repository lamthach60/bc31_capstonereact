import { SETCHITIETPHIM, SETINFORAP } from "../constants/token";
import { quanLyRapService } from "../../services/quanLyRap";
export const layHeThongRapAction = () => {
  return (dispatch) => {
    quanLyRapService
      .layThongTinLichChieuHeThongRap()
      .then((result) => {
        console.log("result", result);
        dispatch({
          type: SETINFORAP,
          payload: result.data.content,
        });
      })
      .catch((err) => {
        console.log("err :", err);
      });
  };
};
export const layThongTinChiTietPhim = (id) => {
  return (dispatch) => {
    quanLyRapService
      .layThongTinLichChieuPhim(id)
      .then((result) => {
        console.log("result :", result);
        dispatch({
          type: SETCHITIETPHIM,
          payload: result.data.content,
        });
      })
      .catch((err) => {
        console.log("err :", err);
      });
  };
};
