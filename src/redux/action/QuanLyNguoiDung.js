import { DANGNHAP, THONGTINNGUOIDUNG } from "../constants/token";
import { nguoiDungService } from "../../services/QuanLyNguoiDung";
import { history } from "../../App";
import { message } from "antd";
export const dangNhapAction = (thongTinDangNhap) => {
  return (dispatch) => {
    nguoiDungService
      .dangNhap(thongTinDangNhap)
      .then((result) => {
        console.log("result :", result);
        dispatch({
          type: DANGNHAP,
          payload: result.data.content,
        });
        message.success(result.data.content);
        history.goBack();
      })
      .catch((err) => {
        console.log("err :", err.response.data);
        message.error(err.response.data);
      });
  };
};
export const layThongTinNguoiDung = () => {
  return (dispatch) => {
    nguoiDungService
      .layThongTinNguoiDung()
      .then((result) => {
        console.log("result :", result);
        dispatch({
          type: THONGTINNGUOIDUNG,
          payload: result.data.content,
        });
      })
      .catch((err) => {
        console.log("err :", err.response.data);
      });
  };
};
