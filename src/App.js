import logo from "./logo.svg";
import { createBrowserHistory } from "history";
import "./App.css";
import { Router, Switch, Route } from "react-router-dom";
import { HomeTemplate } from "./templates/HomeTemplates/HomeTemplate";
import { AdminTemplate } from "./templates/AdminTemplates/AdminTemplate";
import CheckoutTemplates from "./templates/CheckoutTemplates/CheckoutTemplates";
import { UserTemplate } from "./templates/UserTemplate/UserTemplate";
import HomePage from "./pages/Home/HomePage";
import Contact from "./pages/Contact/Contact";
import Login from "./pages/Login/Login";
import Register from "./pages/Register/Register";
import News from "./pages/News/News";
import Detail from "./pages/Detail/Detail";
import Checkout from "./pages/Checkout/Checkout";
import { Suspense, lazy } from "react";
import Loading from "./components/Loading/Loading";
import Profile from "./pages/Profile/Profile";
import Dashboard from "./pages/Admin/Dashboard/Dashboard";
import Films from "./pages/Admin/Films/Films";
import Showtime from "./pages/Admin/Showtime/Showtime";
import AddNew from "./pages/Admin/Films/AddNew/AddNew";
import Edit from "./pages/Edit/Edit";
const CheckoutTemplateLazy = lazy(() =>
  import("./templates/CheckoutTemplates/CheckoutTemplates")
);
export const history = createBrowserHistory();
function App() {
  return (
    <div>
      <Router history={history}>
        <Loading />
        <Switch>
          <AdminTemplate path="/admin" exact Component={Dashboard} />
          <AdminTemplate path="/admin/films" exact Component={Films} />
          <AdminTemplate path="/admin/films/addnew" exact Component={AddNew} />
          <AdminTemplate
            path="/admin/films/showtime/:id/:tenphim"
            exact
            Component={Showtime}
          />
          <AdminTemplate path="/admin/dashboard" exact Component={Dashboard} />
          <AdminTemplate path="/admin/films/edit/:id" exact Component={Edit} />
          <HomeTemplate path="/" exact Component={HomePage} />
          <HomeTemplate path="/home" exact Component={HomePage} />
          <HomeTemplate path="/contact" exact Component={Contact} />
          <HomeTemplate path="/news" exact Component={News} />
          <HomeTemplate path="/detail/:id" exact Component={Detail} />
          <HomeTemplate path="/profile" exact Component={Profile} />
          <CheckoutTemplates path="/checkout/:id" exact Component={Checkout} />
          <UserTemplate path="/login" exact Component={Login}></UserTemplate>
          <UserTemplate
            path="/register"
            exact
            Component={Register}
          ></UserTemplate>
        </Switch>
      </Router>
    </div>
  );
}

export default App;
