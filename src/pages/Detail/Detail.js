import React, { useEffect } from "react";
import "./detail.css";
import "../../assets/style/circle.css";
import { Tabs } from "antd";
import { useDispatch, useSelector } from "react-redux";
import moment from "moment";
import { layThongTinChiTietPhim } from "../../redux/action/quanLyRapAction";
import { Rate } from "antd";
import { NavLink } from "react-router-dom";
export default function Detail(props) {
  const filmDetail = useSelector((state) => state.QuanLyPhimReducer);
  console.log("filmDetail :", { filmDetail });
  console.log("filmDetail.hinhAnh :", filmDetail.hinhAnh);
  const dispatch = useDispatch();
  useEffect(() => {
    let { id } = props.match.params;
    dispatch(layThongTinChiTietPhim(id));
  }, []);

  return (
    <div
      style={{
        backgroundImage: `url(${filmDetail.filmDetail.hinhAnh})`,
        minHeight: "120vh",
        backgroundSize: "100%",
        backgroundPosition: "center",
      }}
    >
      <div
        className="box1 "
        style={{
          paddingTop: 150,
          minHeight: "120vh",
          paddingBottom: "20px",
        }}
      >
        <div className="grid grid-cols-12">
          <div className="col-span-4 col-start-3">
            <div className="grid grid-cols-3">
              <img
                className="col-span-1"
                height={350}
                width={200}
                src={filmDetail.filmDetail.hinhAnh}
              />
              <div className="col-span-2 ml-5  ">
                <p className="text-sm text-white">
                  Ngày chiếu :{" "}
                  {moment(filmDetail.filmDetail.ngayKhoiChieu).format(
                    "DD.MM.YYYY"
                  )}
                </p>
                <p className="text-4xl text-white ">
                  {filmDetail.filmDetail.tenPhim}
                </p>
                <p className="text-white ">{filmDetail.filmDetail.moTa}</p>
              </div>
            </div>
          </div>
          <div className="col-span-4 ml-20">
            <h1
              style={{
                marginLeft: "21%",
                color: "blue",
                fontSize: 20,
              }}
            >
              Đánh giá
            </h1>
            <h1
              style={{
                marginLeft: "15%",
              }}
              className="text-blue-400 text-2xl"
            >
              <Rate allowHalf value={filmDetail.filmDetail.danhGia / 2} />
            </h1>
            <div className={`c100 p${filmDetail.filmDetail.danhGia * 10} big`}>
              <span>{filmDetail.filmDetail.danhGia * 10}%</span>
              <div className="slice">
                <div className="bar" />
                <div className="fill" />
              </div>
            </div>
          </div>
        </div>
        <div className="mt-10 ml-52 w-2/3 bg-white px-5 py-5 container mx-auto">
          <Tabs defaultActiveKey="1" centered>
            <Tabs.TabPane
              tab="Lịch chiếu"
              key="item-1"
              style={{ minHeight: 300 }}
            >
              <div>
                <Tabs defaultActiveKey="1" tabPosition="left">
                  {filmDetail.filmDetail.heThongRapChieu?.map((htr, index) => {
                    return (
                      <Tabs.TabPane
                        tab={
                          <div>
                            <img
                              src={htr.logo}
                              width={50}
                              height={50}
                              className="rounded-full"
                            />
                          </div>
                        }
                        key={index}
                      >
                        {htr.cumRapChieu?.map((cumRap, index) => {
                          return (
                            <div className="mt-5" key={index}>
                              <div className="flex flex-row">
                                <img
                                  src={cumRap.hinhAnh}
                                  width={60}
                                  height={60}
                                />
                                <div className="ml-2 text-lg">
                                  <p className="font-bold">
                                    {cumRap.tenCumRap}
                                  </p>
                                  <p className="text-gray-500">
                                    {cumRap.diaChi}
                                  </p>
                                </div>
                              </div>
                              <div className="thong-tin-lich-chieu grid grid-cols-4">
                                {cumRap.lichChieuPhim
                                  ?.slice(0, 6)
                                  .map((lichChieu, index) => {
                                    return (
                                      <NavLink
                                        to={`/checkout/${lichChieu.maLichChieu}`}
                                        key={index}
                                        className="col-span-1 mt-2 bg-red-600 mr-11 rounded text-center text-white"
                                      >
                                        {moment(
                                          lichChieu.ngayChieuGioChieu
                                        ).format("HH:MM:A")}
                                      </NavLink>
                                    );
                                  })}
                              </div>
                            </div>
                          );
                        })}
                      </Tabs.TabPane>
                    );
                  })}
                </Tabs>
              </div>
            </Tabs.TabPane>
          </Tabs>
        </div>
      </div>
    </div>
  );
}
