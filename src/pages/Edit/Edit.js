import {
  Button,
  Cascader,
  DatePicker,
  Form,
  Input,
  InputNumber,
  Radio,
  Select,
  Switch,
  TreeSelect,
} from "antd";
import { useFormik } from "formik";
import moment from "moment";
import React, { useState } from "react";
import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import {
  capNhatPhimUpload,
  layThongTinPhimAction,
  themPhimUploadHinh,
} from "../../redux/action/movieAction";

const Edit = (props) => {
  const [componentSize, setComponentSize] = useState("default");
  const dispatch = useDispatch();
  const { thongTinPhim } = useSelector((state) => state.QuanLyPhimReducer);
  console.log(" thongTinPhim  :", { thongTinPhim });
  useEffect(() => {
    let { id } = props.match.params;
    dispatch(layThongTinPhimAction(id));
  }, []);

  const onFormLayoutChange = ({ size }) => {
    setComponentSize(size);
  };
  const [imgSrc, setImgSrc] = useState("");
  const formik = useFormik({
    enableReinitialize: true,
    initialValues: {
      maPhim: thongTinPhim.maPhim,
      tenPhim: thongTinPhim?.tenPhim,
      trailer: thongTinPhim?.trailer,
      moTa: thongTinPhim?.moTa,
      ngayKhoiChieu: thongTinPhim?.ngayKhoiChieu,
      dangChieu: thongTinPhim?.dangChieu,
      sapChieu: thongTinPhim?.sapChieu,
      hot: thongTinPhim?.hot,
      danhGia: thongTinPhim?.danhGia,
      hinhAnh: null,
    },
    onSubmit: (values) => {
      console.log("values", values);
      let frm = new FormData();
      for (let key in values) {
        if (key !== "hinhAnh") {
          frm.append(key, values[key]);
        } else {
          if (values.hinhAnh !== null) {
            frm.append("File", values.hinhAnh, values.hinhAnh.name);
          }
        }
      }
      dispatch(capNhatPhimUpload(frm));
    },
  });
  const handleChangeSwitch = (name) => {
    return (value) => {
      formik.setFieldValue(name, value);
    };
  };
  const handleChangeInputNumber = (name) => {
    return (value) => {
      formik.setFieldValue(name, value);
    };
  };
  const handleChangeFile = async (e) => {
    let file = e.target.files[0];

    let reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = (e) => {
      console.log("e.target :", e.target.result);
      setImgSrc(e.target.result);
    };
    await formik.setFieldValue("hinhAnh", file);
  };
  const handleChangeDatePicker = (values) => {
    // console.log("values :", moment(values).format("DD-MM-YYYY"));
    let ngayKhoiChieu = moment(values).format("DD-MM-YYYY");
    formik.setFieldValue("ngayKhoiChieu", ngayKhoiChieu);
  };

  return (
    <>
      <h3 className="text-2xl text-center uppercase font-bold">Edit phim</h3>
      <hr className="mb-3"></hr>
      <Form
        onSubmitCapture={formik.handleSubmit}
        labelCol={{
          span: 4,
        }}
        wrapperCol={{
          span: 14,
        }}
        layout="horizontal"
        initialValues={{
          size: componentSize,
        }}
        onValuesChange={onFormLayoutChange}
        size={componentSize}
      >
        <Form.Item label="Form Size" name="size">
          <Radio.Group>
            <Radio.Button value="small">Small</Radio.Button>
            <Radio.Button value="default">Default</Radio.Button>
            <Radio.Button value="large">Large</Radio.Button>
          </Radio.Group>
        </Form.Item>
        <Form.Item label="Tên Phim">
          <Input
            value={formik.values.tenPhim}
            name="tenPhim"
            onChange={formik.handleChange}
            validate={(values) => {
              let error = {};
              if (!values.tenPhim) {
                console.log("sai");
              }
              return error;
            }}
          />
        </Form.Item>
        <Form.Item label="Trailer">
          <Input
            value={formik.values.trailer}
            name="trailer"
            onChange={formik.handleChange}
          />
        </Form.Item>
        <Form.Item label="Mô Tả">
          <Input
            value={formik.values.moTa}
            name="moTa"
            onChange={formik.handleChange}
          />
        </Form.Item>
        <Form.Item label="Ngày khởi chiếu">
          <DatePicker
            format={"DD-MM-YYYY"}
            onChange={handleChangeDatePicker}
            value={moment(formik.values.ngayKhoiChieu, "DD-MM-YYYY")}
          />
        </Form.Item>
        <Form.Item label="Đang chiếu" valuePropName="checked">
          <Switch
            checked={formik.values.dangChieu}
            name="dangChieu"
            onChange={handleChangeSwitch("dangChieu")}
          />
        </Form.Item>
        <Form.Item label="Hot" valuePropName="checked">
          <Switch
            checked={formik.values.hot}
            name="hot"
            onChange={handleChangeSwitch("hot")}
          />
        </Form.Item>
        <Form.Item label="Sắp chiếu" valuePropName="checked">
          <Switch
            checked={formik.values.sapChieu}
            name="sapChieu"
            onChange={handleChangeSwitch("sapChieu")}
          />
        </Form.Item>
        <Form.Item label="Số sao">
          <InputNumber
            value={formik.values.danhGia}
            onChange={handleChangeInputNumber("danhGia")}
          />
        </Form.Item>
        <Form.Item label="Hình Ảnh">
          <input
            type="file"
            onChange={handleChangeFile}
            accept="image/png, image/gif, image/jpeg, image/jpg"
          />
          <br />
          <img
            width={150}
            height={150}
            src={imgSrc === "" ? thongTinPhim.hinhAnh : imgSrc}
            alt="..."
          />
        </Form.Item>
        <Form.Item label="Tác vụ">
          <button type="submit" className="bg-blue-500 text-white px-2 py-2">
            Cập nhật
          </button>
        </Form.Item>
      </Form>
    </>
  );
};

export default Edit;
