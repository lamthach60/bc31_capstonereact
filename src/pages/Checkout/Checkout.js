import React, { Fragment, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { history } from "../../App";
import { datVeAction, quanLyDatVe } from "../../redux/action/QuanLyDatVeAction";
import "./Checkout.css";
import style from "./Checkout.module.css";
import {
  CloseOutlined,
  UserOutlined,
  SmileOutlined,
  HomeOutlined,
} from "@ant-design/icons";
import { CHANGE_TAB_ACTIVE, DATVE, TOKEN } from "../../redux/constants/token";
import _ from "lodash";
import { Tabs, Pagination, Button } from "antd";
import { ThongTinDatVe } from "../../_core/models/ThongTinDatVe";
import { layThongTinNguoiDung } from "../../redux/action/QuanLyNguoiDung";
import moment from "moment";
import { USERLOGIN } from "../../services/configURL";
import { NavLink } from "react-router-dom";
function Checkout(props) {
  const { userLogin } = useSelector((state) => state.QuanLyNguoiDungReducer);
  const { chiTietPhongVe, danhSachGheDangDat, danhSachGheKhachDat } =
    useSelector((state) => state.QuanLyDatVeReducer);
  console.log("danhSachGheDangDat", { danhSachGheDangDat });
  const { thongTinPhim, danhSachGhe } = chiTietPhongVe;

  let dispatch = useDispatch();
  useEffect(() => {
    dispatch(quanLyDatVe(props.match.params.id));
  }, []);
  const renderSeats = () => {
    return danhSachGhe.map((ghe, index) => {
      let classGheVip = ghe.loaiGhe === "Vip" ? "gheVip" : "";
      let classGheDaDat = ghe.daDat === true ? "gheDaDat" : "";
      let classGheDangDat = "";
      let indexGheDD = danhSachGheDangDat.findIndex(
        (gheDD) => gheDD.maGhe === ghe.maGhe
      );

      if (indexGheDD != -1) {
        classGheDaDat = "gheDangDat";
      }
      let classGheKhachDat = "";
      let indexGheKhachDat = danhSachGheKhachDat.findIndex(
        (gheKD) => gheKD.maGhe === ghe.maGhe
      );
      if (indexGheKhachDat != -1) {
        classGheKhachDat = "gheKhachDat";
      }
      let classGheDaDuocDat = "";
      if (userLogin.taiKhoan === ghe.taiKhoanNguoiDat) {
        classGheDaDuocDat = "gheDaDuocDat";
      }
      return (
        <Fragment>
          <button
            onClick={() => {
              dispatch({
                type: DATVE,
                payload: ghe,
              });
            }}
            disabled={ghe.daDat | (classGheKhachDat !== "")}
            className={`ghe ${classGheKhachDat}  ${classGheVip} ${classGheDaDat} ${classGheDangDat} ${classGheDaDuocDat} text-center `}
            key={index}
          >
            {ghe.daDat ? (
              classGheDaDuocDat != "" ? (
                <UserOutlined />
              ) : (
                <CloseOutlined style={{ fontWeight: "bold" }} />
              )
            ) : classGheKhachDat !== "" ? (
              <SmileOutlined />
            ) : (
              ghe.stt
            )}
          </button>
        </Fragment>
      );
    });
  };

  return (
    <div className=" min-h-screen mt-2">
      <div className="grid grid-cols-12">
        <div className="col-span-9">
          <div className="flex flex-col items-center mt-5">
            <div
              className=" bg-black"
              style={{ width: "80%", height: 15 }}
            ></div>
            <div className={`${style["trapezoid"]} text-center`}>
              <h3 className="mt-3 text-black">Màn Hình</h3>
            </div>
            <div>{renderSeats()}</div>
          </div>
          <div className="mt-5 flex justify-center">
            <table className="divide-y divide-gray-200 w-2/3">
              <thead className="bg-gray-50 p-5">
                <tr>
                  <th>Ghế chưa đặt</th>
                  <th>Ghế đang đặt</th>
                  <th>Ghế VIP</th>
                  <th>Ghế đã được đặt</th>
                  <th>Ghế đặt</th>
                  <th>Ghế khách đang đặt</th>
                </tr>
              </thead>
              <tbody className="bg-white text-center">
                <td>
                  <button className="ghe text-center">00</button>
                </td>
                <td>
                  <button className="ghe gheDangDat text-center">00</button>
                </td>
                <td>
                  <button className="ghe gheVip text-center">00</button>
                </td>
                <td>
                  <button className="ghe gheDaDat text-center">00</button>
                </td>
                <td>
                  <button className="ghe gheDaDuocDat text-center">00</button>
                </td>
                <td>
                  <button className="ghe gheKhachDat text-center">00</button>
                </td>
              </tbody>
            </table>
          </div>
        </div>
        <div className="col-span-3">
          <h3 className="text-center text-2xl text-green-800">
            {" "}
            {danhSachGheDangDat
              .reduce((tongTien, ghe, index) => {
                return (tongTien += ghe.giaVe);
              }, 0)
              .toLocaleString()}{" "}
            đ
          </h3>
          <hr />
          <br></br>
          <h3 className="text-xl font-bold">{thongTinPhim?.tenPhim}</h3>
          <p>{thongTinPhim?.tenCumRap}</p>
          <p>
            Ngày Chiếu: {thongTinPhim?.ngayChieu} - {thongTinPhim?.gioChieu}
          </p>
          <br></br>
          <hr />
          <div className="flex flex-row my-5">
            <div className="min-w-[50%] break-all">
              <span className="text-red-500">Ghế</span>
              {_.sortBy(
                danhSachGheDangDat.map((gheDD, index) => {
                  return (
                    <span key={index} className="text-green-700 text-xl ml-2">
                      {gheDD.stt}
                    </span>
                  );
                })
              )}
            </div>
          </div>

          <div>
            <p>Giá vé: </p>
            {danhSachGheDangDat
              .reduce((tongTien, ghe, index) => {
                return (tongTien += ghe.giaVe);
              }, 0)
              .toLocaleString()}
          </div>
          <hr></hr>
          <div className="my-5">
            <i>Email :</i>
            <br />
            {userLogin.email}
          </div>
          <div className="my-5">
            <i>Phone :</i>
            <br />
            {userLogin.soDT}
          </div>
          <hr></hr>
          <div
            className="mb-0 h-full flex flex-col items-center"
            style={{ paddingBottom: 50 }}
          >
            <div
              className="bg-red-500 text-white w-full text-center py-2"
              onClick={() => {
                const thongTinDatVe = new ThongTinDatVe();
                thongTinDatVe.maLichChieu = props.match.params.id;
                thongTinDatVe.danhSachVe = danhSachGheDangDat;
                console.log("thongTinDatVe :", thongTinDatVe);
                dispatch(datVeAction(thongTinDatVe));
              }}
            >
              Đặt vé
            </div>{" "}
          </div>
        </div>
      </div>
    </div>
  );
}

export default function CheckoutTab(props) {
  const { userLogin } = useSelector((state) => state.QuanLyNguoiDungReducer);
  const operations = (
    <Fragment>
      {!_.isEmpty(userLogin) ? (
        <div>
          <button
            onClick={() => {
              history.push("/profile");
            }}
          >
            Hello{" "}
            <span className="text-red-500 font-bold mr-2">
              {userLogin.taiKhoan}
            </span>
          </button>
          <span>|</span>
          <button
            className="text-blue-500 ml-2"
            onClick={() => {
              localStorage.removeItem(USERLOGIN);
              localStorage.removeItem(TOKEN);
              history.push("/home");
              window.location.reload();
            }}
          >
            Đăng xuất
          </button>
        </div>
      ) : (
        ""
      )}
    </Fragment>
  );

  const { tabActive } = useSelector((state) => state.QuanLyDatVeReducer);

  console.log("{ tabActive } :", { tabActive });
  let dispatch = useDispatch();
  function callback(key) {
    console.log(key);
  }

  return (
    <div className="p-5">
      <Tabs
        tabBarExtraContent={operations}
        defaultActiveKey="2"
        activeKey={tabActive.toString()}
        onChange={(key) => {
          console.log("key", key);
          dispatch({
            type: CHANGE_TAB_ACTIVE,
            payload: key,
          });
        }}
      >
        <Tabs.TabPane tab="CHỌN GHẾ THANH TOÁN" key="1">
          <Checkout {...props} />
        </Tabs.TabPane>
        <Tabs.TabPane tab="KẾT QUẢ ĐẶT VÉ" key="2">
          <KetQuaDatVe {...props} />
        </Tabs.TabPane>
        <Tabs.TabPane
          tab={
            <div className="text-center ">
              <NavLink to="/">TRANG CHỦ</NavLink>
            </div>
          }
          key="3"
        ></Tabs.TabPane>
      </Tabs>
    </div>
  );

  function KetQuaDatVe(props) {
    let dispatch = useDispatch();
    const { thongTinNguoiDung } = useSelector(
      (state) => state.QuanLyNguoiDungReducer
    );
    console.log("{ thongTinNguoiDung } :", { thongTinNguoiDung });
    const { userLogin } = useSelector((state) => state.QuanLyNguoiDungReducer);
    useEffect(() => {
      dispatch(layThongTinNguoiDung());
    }, []);
    const renderTicketItem = () => {
      return thongTinNguoiDung.thongTinDatVe?.map((ticket, index) => {
        const seats = _.first(ticket.danhSachGhe);
        return (
          <div className="p-2 lg:w-1/3 md:w-1/2 w-full" key={index}>
            <div className="h-full flex items-center border-gray-200 border p-4 rounded-lg">
              <img
                alt="team"
                className="w-16 h-16 bg-gray-100 object-cover object-center flex-shrink-0 rounded-full mr-4"
                src={ticket.hinhAnh}
              />
              <div className="flex-grow">
                <h2 className="text-gray-900 title-font font-medium">
                  {ticket.tenPhim}
                </h2>
                <p className="text-gray-500">CGV - Vincom Landmark 81</p>
                <p className="text-gray-500">
                  Ngày Chiếu: {moment(ticket.ngayDat).format("DD-MM-YYYY")}
                </p>
                <p className="text-gray-500">
                  Giờ Chiếu: {moment(ticket.ngayDat).format("HH:MM A")}
                </p>
                <p className="text-gray-500">
                  Địa Điểm: {seats.tenHeThongRap} - {seats.tenCumRap} - Ghế{" "}
                  {ticket.danhSachGhe.map((ghe, index) => {
                    return <span key={index}>[{ghe.tenGhe}]</span>;
                  })}
                </p>
              </div>
            </div>
          </div>
        );
      });
    };
    return (
      <div className="p-5">
        <section className="text-gray-600 body-font">
          <div className="container px-5 py-24 mx-auto">
            <div className="flex flex-col text-center w-full mb-20">
              <h1 className="sm:text-3xl text-2xl font-medium title-font mb-4 text-gray-900">
                Lịch sử đặt vé của khách hàng
              </h1>
              <p className="lg:w-2/3 mx-auto leading-relaxed text-base">
                Hãy xem thông tin địa điểm và thời gian để xem phim vui vẻ bạn
                nhé!
              </p>
            </div>
            <div className="flex flex-wrap -m-2">{renderTicketItem()}</div>
          </div>
        </section>
      </div>
    );
  }
}
