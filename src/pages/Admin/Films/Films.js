import React from "react";
import { Table } from "antd";
import {
  CalendarOutlined,
  EditOutlined,
  DeleteOutlined,
  SearchOutlined,
} from "@ant-design/icons";
import { Input, Space } from "antd";
import { useDispatch, useSelector } from "react-redux";
import { useEffect } from "react";
import {
  layDanhSachPhim,
  xoaPhimAction,
} from "../../../redux/action/movieAction";
import { Button } from "antd";
import { Fragment } from "react";
import { NavLink } from "react-router-dom";
import { history } from "../../../App";
export default function Films() {
  const { Search } = Input;
  const columns = [
    {
      title: "Mã Phim",
      dataIndex: "maPhim",

      // specify the condition of filtering result
      // here is that finding the name started with `value`
      onFilter: (value, record) => record.maPhim.indexOf(value) === 0,
      sorter: (a, b) => a.maPhim.length - b.maPhim.length,
      sortDirections: ["descend", "ascend"],
      width: "10%",
    },
    {
      title: "Hình Ảnh",
      dataIndex: "hinhAnh",
      render: (text, film) => {
        return (
          <Fragment>
            <img src={film.hinhAnh} width={50} height={50} />
          </Fragment>
        );
      },
      width: "15%",
      onFilter: (value, record) => record.address.indexOf(value) === 0,
    },
    {
      title: "Tên Phim",
      dataIndex: "tenPhim",
      defaultSortOrder: "descend",
      width: "20%",
      sorter: (a, b) => a.age - b.age,
    },
    {
      title: "Mô tả",
      dataIndex: "moTa",
      width: "20%",
      render: (text, film) => {
        return (
          <Fragment>
            {film.moTa.length > 50
              ? film.moTa.substr(0, 50) + "..."
              : film.moTa}
          </Fragment>
        );
      },
      onFilter: (value, record) => record.address.indexOf(value) === 0,
    },
    {
      title: "Hành Động",
      dataIndex: "maPhim",
      width: "20%",
      render: (text, film) => {
        return (
          <Fragment>
            <NavLink
              key={1}
              className="text-blue-700 mr-4 text-2xl"
              to={`/admin/films/edit/${film.maPhim}`}
            >
              <EditOutlined />
            </NavLink>
            <span
              style={{ cursor: "pointer" }}
              onClick={() => {
                if (
                  window.confirm(
                    "Bạn có chắc muốn xóa " + film.tenPhim + " không ?"
                  )
                ) {
                  dispatch(xoaPhimAction(film.maPhim));
                }
              }}
              key={2}
              className="text-red-700 text-2xl"
              to="/"
            >
              <DeleteOutlined />
            </span>
            <NavLink
              key={3}
              className="text-blue-700 ml-4 text-2xl"
              to={`/admin/films/showtime/${film.maPhim}/${film.tenPhim}`}
              onClick={() => {
                localStorage.setItem("filmParams", JSON.stringify(film));
              }}
            >
              <CalendarOutlined />
            </NavLink>
          </Fragment>
        );
      },
      onFilter: (value, record) => record.address.indexOf(value) === 0,
    },
  ];
  const { arrFilmDefault } = useSelector((state) => state.QuanLyPhimReducer);
  const data = arrFilmDefault;
  const onChange = (pagination, filters, sorter, extra) => {
    console.log("params", pagination, filters, sorter, extra);
  };
  const onSearch = (value) => {
    dispatch(layDanhSachPhim(value));
  };

  console.log("arrFilmDefault :", arrFilmDefault);
  let dispatch = useDispatch();
  useEffect(() => {
    dispatch(layDanhSachPhim());
  }, []);

  return (
    <div>
      <h3 className="text-2xl font-bold mb-2 uppercase">Quản lý phim</h3>
      <Button
        onClick={() => {
          history.push("/admin/films/addnew");
        }}
        className="bg-black mb-2 rounded-lg text-white"
      >
        Thêm phim
      </Button>
      <Search
        placeholder="input search text"
        allowClear
        size="large"
        onSearch={onSearch}
        enterButton={<SearchOutlined className="text-white" />}
      />
      <Table columns={columns} dataSource={data} onChange={onChange} />
    </div>
  );
}
