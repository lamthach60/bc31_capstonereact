import React, { Fragment } from "react";
import { Tabs, TabPane } from "antd";
import { NavLink } from "react-router-dom";
import moment from "moment";

export default function HomeMenu(props) {
  const renderHeThongRap = () => {
    return props.heThongRapChieu.map((heThongRap, index) => {
      return (
        <Tabs.TabPane
          tab={
            <img src={heThongRap.logo} className="rounded-full" width={70} />
          }
          key={index}
        >
          <Tabs defaultActiveKey="1" tabPosition="left">
            {heThongRap.lstCumRap.map((cumRap, index) => {
              return (
                <Tabs.TabPane
                  tab={
                    <div style={{ width: "300px", display: "flex" }}>
                      <img src={cumRap.hinhAnh} width={80} />
                      <br></br>
                      <div className="text-left ml-3">
                        <p className="font-bold nameRap">{cumRap.tenCumRap}</p>
                        <p className="diaChiRap">{cumRap.diaChi}</p>
                        <p className="text-red-500">Chi tiết</p>
                      </div>
                    </div>
                  }
                  key={index}
                >
                  {cumRap.danhSachPhim.slice(0, 4).map((phim, index) => {
                    return (
                      <Fragment key={index}>
                        <div className="mb-2">
                          <div style={{ display: "flex" }}>
                            <img src={phim.hinhAnh} width={100} height={100} />
                            <div className="ml-2">
                              <h1 className="text-xl font-bold ">
                                {phim.tenPhim}
                              </h1>
                              <div className="grid grid-cols-6 gap-5 mt-1">
                                {phim.lstLichChieuTheoPhim
                                  ?.slice(0, 12)
                                  .map((lichChieu, index) => {
                                    return (
                                      <NavLink
                                        className="bg-red-500 text-white py-2 px-2 rounded"
                                        to={`/checkout/${lichChieu.maLichChieu}`}
                                        key={index}
                                      >
                                        {moment(
                                          lichChieu.ngayChieuGioChieu
                                        ).format("hh:mm")}
                                      </NavLink>
                                    );
                                  })}
                              </div>
                            </div>
                          </div>
                        </div>
                      </Fragment>
                    );
                  })}
                </Tabs.TabPane>
              );
            })}
          </Tabs>
        </Tabs.TabPane>
      );
    });
  };
  console.log("props :", props);
  return (
    <div>
      <Tabs defaultActiveKey="1" tabPosition="left">
        {renderHeThongRap()}
      </Tabs>
    </div>
  );
}
