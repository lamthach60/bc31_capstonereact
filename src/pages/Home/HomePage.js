import React, { useEffect } from "react";
import HomeMenu from "./HomeMenu/HomeMenu";
//kết nối redux
import { useSelector, useDispatch } from "react-redux";
import { layDanhSachPhim } from "../../redux/action/movieAction";
import MultipleRows from "../../components/RSFlick/MultipleRow";
import { layHeThongRapAction } from "../../redux/action/quanLyRapAction";
import HomeCarousel from "../../templates/HomeTemplates/Layout/HomeCarousel/HomeCarousel";

export default function HomePage(props) {
  let dispatch = useDispatch();
  const { heThongRapChieu } = useSelector((state) => {
    return state.QuanLyRapReducer;
  });
  console.log("heThongRapChieu  :", heThongRapChieu);
  let { arrFilm } = useSelector((state) => {
    return state.QuanLyPhimReducer;
  });

  useEffect(() => {
    dispatch(layDanhSachPhim());
    dispatch(layHeThongRapAction());
  }, []);
  // let renderMovieList = () => {
  //   return arrFilm.map((item, index) => {
  //     return <Films key={index} />;
  //   });
  // };
  return (
    <div className="container">
      <HomeCarousel />
      <section className="text-gray-600 body-font  ">
        <div className=" px-5 py-24 mx-auto">
          <MultipleRows arrFilm={arrFilm} />
          {/* <div className="flex flex-wrap -m-4"> {renderMovieList()}</div> */}
        </div>
      </section>
      <div className="mx-36 heThongRap">
        <HomeMenu heThongRapChieu={heThongRapChieu} />
      </div>
    </div>
  );
}
