import React, { Fragment } from "react";
import { LoadingOutlined } from "@ant-design/icons";
import { Spin } from "antd";
import { useSelector } from "react-redux";

export default function Loading() {
  const antIcon = (
    <LoadingOutlined style={{ fontSize: 100, color: "white" }} spin />
  );
  const { isLoading } = useSelector((state) => state.LoadingReducer);
  return (
    <Fragment>
      {isLoading ? (
        <div
          style={{
            postion: "fixed",
            top: 0,
            left: 0,
            width: "100%",
            height: "100%",
            backgroundColor: "rgba(0,0,0,.5)",
            justifyContent: "center",
            alignItems: "center",
            display: "flex",
            zIndex: 99,
            position: "fixed",
          }}
        >
          <div className="text-4xl text-white">
            <Spin indicator={antIcon} />
          </div>
        </div>
      ) : (
        ""
      )}
    </Fragment>
  );
}
