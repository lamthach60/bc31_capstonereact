import { data } from "autoprefixer";
import React, { Component } from "react";
import { useDispatch, useSelector } from "react-redux";
import Slider from "react-slick";
import { SETPHIMDANGCHIEU, SETPHIMSAPCHIEU } from "../../redux/constants/token";
import Film_Flip from "../Films/Film_Flip";
import styleSlick from "./Multiple.module.css";
function SampleNextArrow(props) {
  const { className, style, onClick } = props;

  return (
    <div
      className={`${className} ${styleSlick["slick-next"]}`}
      style={{ ...style, display: "block" }}
      onClick={onClick}
    />
  );
}

function SamplePrevArrow(props) {
  const { className, style, onClick } = props;
  return (
    <div
      className={`${className} ${styleSlick["slick-prev"]}`}
      style={{ ...style, display: "block" }}
      onClick={onClick}
    />
  );
}
const MultipleRows = (props) => {
  const dispatch = useDispatch();
  const { dangChieu, sapChieu } = useSelector(
    (state) => state.QuanLyPhimReducer
  );
  let activeClass = dangChieu === true ? "none_active_film" : "active_film";
  console.log("activeClass :", activeClass);
  let noneactiveClass = sapChieu == true ? "none_active_film" : "active_film";
  const renderFilms = () => {
    return props.arrFilm.slice(0, 5).map((item, index) => {
      return (
        <div className="mb-3 container">
          {/* <Films key={index} phim={item} /> */}
          <Film_Flip item={item} />
        </div>
      );
    });
  };

  const settings = {
    className: "center ",
    centerMode: true,
    infinite: true,
    centerPadding: "-200px",
    slidesToShow: 1,
    speed: 500,
    rows: 1,
    slidesPerRow: 5,
  };
  return (
    <div className="container">
      <button
        type="button"
        className={`${styleSlick[activeClass]} px-8 py-3 font-semibold border rounded uppercase`}
        onClick={() => {
          const action = { type: SETPHIMDANGCHIEU, payload: data.dangChieu };
          dispatch(action);
        }}
      >
        Phim đang chiếu
      </button>
      <button
        type="button"
        className={`${styleSlick[noneactiveClass]} px-8 py-3 font-semibold border rounded uppercase`}
        onClick={() => {
          const action = { type: SETPHIMSAPCHIEU, payload: data.sapChieu };
          dispatch(action);
        }}
      >
        Phim sắp chiếu
      </button>
      <Slider {...settings}>{renderFilms()}</Slider>
    </div>
  );
};

export default MultipleRows;
