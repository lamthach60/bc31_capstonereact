import React from "react";
import "./Film_Flip.css";
import { Icon } from "@iconify/react";
import { NavLink } from "react-router-dom";
import { history } from "../../App";
export default function Film_Flip(props) {
  const { item } = props;
  return (
    <div className="flip-card mt-2 Film_Slider">
      <div className="flip-card-inner  mx-2">
        <div className="flip-card-front ">
          <img src={item.hinhAnh} alt="Avatar" className="img-FilmFlip" />
        </div>
        <div
          className="flip-card-back"
          style={{
            position: "relative",
            backgroundColor: "rgba(0,0,0,.9)",
          }}
        >
          <div style={{ position: "absolute", top: 0, left: 0 }}>
            <img
              src={item.hinhAnh}
              alt="Avatar"
              style={{ width: 300, height: 300 }}
            />
          </div>
          <div
            className="w-full h-full"
            style={{
              position: "absolute",
              backgroundColor: "rgba(0,0,0,.5)",
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
            }}
          >
            <div>
              <div className="rounded-full cursor-pointer">
                <Icon
                  icon="mdi:play-circle-outline"
                  style={{ fontSize: "50px" }}
                />
                <div className="text-2xl mt-2 font-bold">{item.tenPhim}</div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div>
        <div
          onClick={() => {
            history.push(`/detail/${item.maPhim}`);
          }}
          className="w-full text-center cursor-pointer mt-2 uppercase text-black hover:text-yellow-500 font-bold"
        >
          Đặt vé
        </div>
      </div>
    </div>
  );
}
