import { Fragment, useEffect } from "react";
import { Route } from "react-router-dom";
import Footer from "./Layout/Footer/Footer";
import Header from "./Layout/Header/Header";
import Introduce from "./Layout/Introduce/Introduce";

export const HomeTemplate = (props) => {
  //props, exact,component
  const { Component, ...restProps } = props;
  useEffect(() => {
    window.scrollTo(0, 0);
  });

  return (
    <Route
      {...restProps}
      render={(propsRoute) => {
        return (
          <Fragment>
            <Header {...propsRoute} />

            <Component {...propsRoute} />
            <Introduce />
            <Footer />
          </Fragment>
        );
      }}
    />
  );
};
