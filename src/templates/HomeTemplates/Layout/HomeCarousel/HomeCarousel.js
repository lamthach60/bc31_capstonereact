import React, { useEffect } from "react";
import { Carousel } from "antd";
import { useSelector, useDispatch } from "react-redux";
import { getCarouselAction } from "../../../../redux/action/CarouselAction";
import "./HomeCarousel.css";
export default function HomeCarousel(props) {
  let { arrImg } = useSelector((state) => {
    return state.CarouselReducer;
  });

  let dispatch = useDispatch();
  //sẽ tự kích hoạt khi component xuất hiện
  useEffect(() => {
    dispatch(getCarouselAction());
  }, []);

  const contentStyle = {
    height: "800px",
    color: "#fff",
    lineHeight: "160px",
    textAlign: "center",
    background: "#364d79",
    backgroundPosition: "contain",
    backgroundSize: "100%",
    backgroundRepeat: "no-repeat",
  };
  let renderImg = () => {
    return arrImg.map((item) => {
      return (
        <div>
          <div
            style={{ ...contentStyle, backgroundImage: `url(${item.hinhAnh})` }}
          >
            <img src={item.hinhAnh} className="w-full opacity-0 " />
          </div>
        </div>
      );
    });
  };
  return (
    <div>
      <Carousel autoplay style={{ position: "relative", zIndex: "1" }}>
        {renderImg()}
      </Carousel>
    </div>
  );
}
