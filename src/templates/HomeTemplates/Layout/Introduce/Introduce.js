import React from "react";

export default function Introduce() {
  return (
    <div
      className="introduce relative "
      style={{
        background:
          "url(http://demo1.cybersoft.edu.vn/static/media/backapp.b46ef3a1.jpg)",
        height: "120vh",
      }}
    >
      <div className="text-center py-60">
        <div className="">
          <h2 className="text-white font-bold text-4xl uppercase">
            Ứng dụng tiện lợi dành cho người yêu điện ảnh
          </h2>
          <br></br>
          <p className="text-white text-lg ">
            Không chỉ đặt vé, bạn còn có thể bình luận phim, chấm điểm rạp và
            đổi quà hấp dẫn.
          </p>
          <button className="bg-red-500 py-3 px-5 mt-5 text-white rounded">
            Tải ngay
          </button>
        </div>
      </div>
    </div>
  );
}
