import {
  DashboardOutlined,
  FundOutlined,
  FieldTimeOutlined,
} from "@ant-design/icons";
import { Breadcrumb, Layout, Menu } from "antd";
import { NavLink, Route } from "react-router-dom";
import React, { useState } from "react";
import { useEffect } from "react";
import SubMenu from "antd/lib/menu/SubMenu";
import Film_Flip from "../../components/Films/Film_Flip";
const { Header, Content, Footer, Sider } = Layout;

function getItem(label, key, icon, children) {
  return {
    key,
    icon,
    children,
    label,
  };
}

export const AdminTemplate = (props) => {
  //props, exact,component
  const [collapsed, setCollapsed] = useState(false);
  const { Component, ...restProps } = props;
  useEffect(() => {
    window.scrollTo(0, 0);
  });

  return (
    <Route
      {...restProps}
      render={(propsRoute) => {
        return (
          <Layout
            style={{
              minHeight: "100vh",
            }}
          >
            <Sider
              collapsible
              collapsed={collapsed}
              onCollapse={(value) => setCollapsed(value)}
            >
              <div className="logo">
                <img src="https://cyberlearn.vn/wp-content/uploads/2020/03/cyberlearn-min-new-opt2.png" />
              </div>
              <Menu theme="dark" defaultSelectedKeys={["1"]} mode="inline">
                <SubMenu key="sub1" icon={<FundOutlined />} title="Films">
                  <Menu.Item key="2" icon={<FundOutlined />}>
                    <NavLink to={"/admin/films"}>Films</NavLink>
                  </Menu.Item>
                  <Menu.Item icon={<FundOutlined />}>
                    <NavLink to={"/admin/films/addnew"}>Add New </NavLink>
                  </Menu.Item>
                </SubMenu>
                <Menu.Item icon={<FieldTimeOutlined />}>
                  <NavLink to={"/admin/Showtime"}>Showtime</NavLink>
                </Menu.Item>
              </Menu>
            </Sider>
            <Layout className="site-layout">
              <Header
                className="site-layout-background"
                style={{
                  padding: 0,
                }}
              />
              <Content
                style={{
                  margin: "0 16px",
                }}
              >
                <Breadcrumb
                  style={{
                    margin: "16px 0",
                  }}
                >
                  {/* <Breadcrumb.Item>User</Breadcrumb.Item>
                  <Breadcrumb.Item>Bill</Breadcrumb.Item> */}
                </Breadcrumb>
                <div className="site-layout-background">
                  <Component {...propsRoute} />
                </div>
              </Content>
              <Footer
                style={{
                  textAlign: "center",
                }}
              >
                Ant Design ©2018 Created by Ant UED
              </Footer>
            </Layout>
          </Layout>
        );
      }}
    />
  );
};
