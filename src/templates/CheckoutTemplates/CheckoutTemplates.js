import { Fragment, useEffect } from "react";
import { Redirect, Route } from "react-router-dom";
import { USERLOGIN } from "../../services/configURL";

const CheckoutTemplates = (props) => {
  useEffect(() => {
    window.scrollTo(0, 0);
  });
  //props, exact,component
  const { Component, ...restProps } = props;
  if (!localStorage.getItem(USERLOGIN)) {
    return <Redirect to="/login" />;
  }

  return (
    <Route
      {...restProps}
      render={(propsRoute) => {
        return (
          <Fragment>
            <Component {...propsRoute} />
          </Fragment>
        );
      }}
    />
  );
};
export default CheckoutTemplates;
