import { https } from "./configURL";
export let movieService = {
  getBanner: () => {
    return https.get("/api/QuanLyPhim/LayDanhSachBanner");
  },
  layDanhSachPhim: (tenPhim) => {
    if (tenPhim.trim() != "") {
      return https.get(
        `/api/QuanLyPhim/LayDanhSachPhim?maNhom=GP01&tenPhim=${tenPhim}`
      );
    }
    return https.get("/api/QuanLyPhim/LayDanhSachPhim?maNhom=GP01");
  },
  themPhimUploadHinh: (frm) => {
    return https.post(`/api/QuanLyPhim/ThemPhimUploadHinh`, frm);
  },
  layThongTinPhim: (maPhim) => {
    return https.get(`/api/QuanLyPhim/LayThongTinPhim?MaPhim=${maPhim}`);
  },
  capNhatPhimUpload: (frm) => {
    return https.post(`/api/QuanLyPhim/CapNhatPhimUpload`, frm);
  },
  xoaPhim: (maPhim) => {
    return https.delete(`/api/QuanLyPhim/XoaPhim?MaPhim=${maPhim}`);
  },
};
