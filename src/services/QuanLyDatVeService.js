import { ThongTinDatVe } from "../_core/models/ThongTinDatVe";
import { https } from "./configURL";
export let QuanLyDatVeService = {
  layChiTietPhongVe: (maLichChieu) => {
    return https.get(
      `/api/QuanLyDatVe/LayDanhSachPhongVe?MaLichChieu=${maLichChieu}`
    );
  },
  datVe: (thongTinDatVe = new ThongTinDatVe()) => {
    return https.post(`/api/QuanLyDatVe/DatVe`, thongTinDatVe);
  },
  taoLichChieu: (thongTinLichChieu) => {
    return https.post(`/api/QuanLyDatVe/TaoLichChieu`, thongTinLichChieu);
  },
};
