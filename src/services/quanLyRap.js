import { https } from "./configURL";
export let quanLyRapService = {
  layThongTinLichChieuHeThongRap: () => {
    return https.get(
      "api/QuanLyRap/LayThongTinLichChieuHeThongRap?maNhom=GP01"
    );
  },
  layThongTinLichChieuPhim: (maPhim) => {
    return https.get(
      `/api/QuanLyRap/LayThongTinLichChieuPhim?MaPhim=${maPhim}`
    );
  },
  layThongTinHeThongRap: (first) => {
    return https.get(`/api/QuanLyRap/LayThongTinHeThongRap`);
  },
  layThongTinCumRap: (maHeThongRap) => {
    return https.get(
      `/api/QuanLyRap/LayThongTinCumRapTheoHeThong?maHeThongRap=${maHeThongRap}`
    );
  },
};
