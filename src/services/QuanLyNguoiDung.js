import { https } from "./configURL";
export let nguoiDungService = {
  dangNhap: (thongTinDangNhap) => {
    return https.post("/api/QuanLyNguoiDung/DangNhap", thongTinDangNhap);
  },
  layThongTinNguoiDung: () => {
    return https.post("/api/QuanLyNguoiDung/ThongTinTaiKhoan");
  },
};
