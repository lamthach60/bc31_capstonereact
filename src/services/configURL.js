import axios from "axios";
import { TOKEN, TOKEN_CYBERSOFT } from "../redux/constants/token";

export let https = axios.create({
  baseURL: "https://movienew.cybersoft.edu.vn",
  headers: {
    TokenCybersoft: TOKEN_CYBERSOFT,
    Authorization: "Bearer " + localStorage.getItem(TOKEN),
  },
});
export const USERLOGIN = "USERLOGIN";
